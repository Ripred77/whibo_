package com.bolsadeideas.springboot.app.entity;


public class HelperProveedorPrecio {
	
	private Long ProductoProveedorID;
	
	private String NombreProveedor;
	
	private String Direccion;
	
	private int Precio;

	public String getNombreProveedor() {
		return NombreProveedor;
	}

	public void setNombreProveedor(String nombreProveedor) {
		NombreProveedor = nombreProveedor;
	}

	public String getDireccion() {
		return Direccion;
	}

	public void setDireccion(String direccion) {
		Direccion = direccion;
	}

	public int getPrecio() {
		return Precio;
	}

	public void setPrecio(int precio) {
		Precio = precio;
	}

	public Long getProductoProveedorID() {
		return ProductoProveedorID;
	}

	public void setProductoProveedorID(Long productoProveedorID) {
		ProductoProveedorID = productoProveedorID;
	}

	
	
	
	

	
}
