package com.bolsadeideas.springboot.app.entity.Cliente;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.persistence.Id;

@Entity
@Table(name="factura")
public class Factura {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long Id;
	
	@NotEmpty
	@Column(name="neto")
	private int neto;
	
	@NotEmpty
	@Column(name="IVA")
	private int IVA;
	
	@NotEmpty
	@Column(name="total")
	private int total;
	
	@NotEmpty
	@Column(name="exento")
	private Boolean Exento;
	
	@NotEmpty
	@Column(name="descripcion")
	private String Descripcion;

	
	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public int getNeto() {
		return neto;
	}

	public void setNeto(int neto) {
		this.neto = neto;
	}

	public int getIVA() {
		return IVA;
	}

	public void setIVA(int iVA) {
		IVA = iVA;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public Boolean getExento() {
		return Exento;
	}

	public void setExento(Boolean exento) {
		Exento = exento;
	}

	public String getDescripcion() {
		return Descripcion;
	}

	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}
	
}
