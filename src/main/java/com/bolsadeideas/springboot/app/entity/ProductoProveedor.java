package com.bolsadeideas.springboot.app.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
/*@Table(
        name="producto_proveedor",
        uniqueConstraints=
            @UniqueConstraint(columnNames={"producto_id", "proveedor_id"})
    )*/
public class ProductoProveedor implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long Id;
	
	@NotNull
	private Long producto_id;
	
	@NotNull
	private Long proveedor_id;

	@NotNull
	private int Neto;
	
	@NotNull
	private int IVA;
	
	@NotNull
	private int Total;
	
	@NotNull
	@Column(name="valor_agregado")
	private int ValorAgregado;
	
	public Long getProducto_id() {
		return producto_id;
	}

	public void setProducto_id(Long producto_id) {
		this.producto_id = producto_id;
	}

	public Long getProveedor_id() {
		return proveedor_id;
	}

	public void setProveedor_id(Long proveedor_id) {
		this.proveedor_id = proveedor_id;
	}

	private static final long serialVersionUID = 1L;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public int getNeto() {
		return Neto;
	}

	public void setNeto(int neto) {
		Neto = neto;
	}

	public int getIVA() {
		return IVA;
	}

	public void setIVA(int iVA) {
		IVA = iVA;
	}

	public int getTotal() {
		return Total;
	}

	public void setTotal(int total) {
		Total = total;
	}

	public int getValorAgregado() {
		return ValorAgregado;
	}

	public void setValorAgregado(int valorAgregado) {
		ValorAgregado = valorAgregado;
	}

}
