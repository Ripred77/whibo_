package com.bolsadeideas.springboot.app.entity.Cliente;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name="etiquetas")
public class Etiqueta implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotEmpty
	@Column(name="nombre_etiqueta")
	private String nombreEtiqueta;
	
	@OneToMany(fetch = FetchType.LAZY , cascade = CascadeType.ALL)
	@JoinColumn(name = "etiqueta_id")
	List<RegistroEtiqueta> registroEtiqueta;
	
	private static final long serialVersionUID = 1L;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombreEtiqueta() {
		return nombreEtiqueta;
	}

	public List<RegistroEtiqueta> getRegistroEtiqueta() {
		return registroEtiqueta;
	}

	public void setRegistroEtiqueta(List<RegistroEtiqueta> registroEtiqueta) {
		this.registroEtiqueta = registroEtiqueta;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setNombreEtiqueta(String nombreEtiqueta) {
		this.nombreEtiqueta = nombreEtiqueta;
	}



}
