package com.bolsadeideas.springboot.app.entity.Cliente;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="registros_etiquetas")
public class RegistroEtiqueta implements Serializable{	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotNull
	private Long producto_id;
	
	@NotNull
	private Long etiqueta_id;
	
	private static final long serialVersionUID = 1L;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getEtiqueta_id() {
		return etiqueta_id;
	}

	public void setEtiqueta_id(Long etiqueta_id) {
		this.etiqueta_id = etiqueta_id;
	}

	public Long getProducto_id() {
		return producto_id;
	}

	public void setProducto_id(Long producto_id) {
		this.producto_id = producto_id;
	}
	
	
	
}
