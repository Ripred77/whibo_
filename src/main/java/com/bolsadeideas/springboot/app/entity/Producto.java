package com.bolsadeideas.springboot.app.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.bolsadeideas.springboot.app.entity.Cliente.RegistroEtiqueta;

@Entity
@Table(name="productos")
public class Producto implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long Id;
	
	@NotEmpty
	@Column(name="nombre_producto")
	private String nombreProducto;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "producto_id")
	List<ProductoProveedor> productoProveedor;
	
	@OneToMany(fetch = FetchType.LAZY , cascade = CascadeType.ALL)
	@JoinColumn(name = "producto_id")
	List<RegistroEtiqueta> registroEtiqueta;
	
	private String foto;
	
	private String descripcion;
	
	@NotNull
	@Column(name="total_venta")
	private int TotalVenta;
	
	public int getTotalVenta() {
		return TotalVenta;
	}

	public void setTotalVenta(int totalVenta) {
		TotalVenta = totalVenta;
	}

	private static final long serialVersionUID = 1L;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getNombreProducto() {
		return nombreProducto;
	}

	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}
	
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<ProductoProveedor> getProductoProveedor() {
		return productoProveedor;
	}

	public void setProductoProveedor(List<ProductoProveedor> productoProveedor) {
		this.productoProveedor = productoProveedor;
	}
	
}
