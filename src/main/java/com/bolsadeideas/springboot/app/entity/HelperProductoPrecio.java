package com.bolsadeideas.springboot.app.entity;

public class HelperProductoPrecio {

	private Long ProductoProveedorID;
	
	private String NombreProducto;
	
	private int Neto;
	
	private String foto;
	
	private String descripcion;

	public String getNombreProducto() {
		return NombreProducto;
	}

	public void setNombreProducto(String nombreProducto) {
		NombreProducto = nombreProducto;
	}

	public int getNeto() {
		return Neto;
	}

	public void setNeto(int neto) {
		Neto = neto;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public Long getProductoProveedorID() {
		return ProductoProveedorID;
	}

	public void setProductoProveedorID(Long productoProveedorID) {
		ProductoProveedorID = productoProveedorID;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
	
}
