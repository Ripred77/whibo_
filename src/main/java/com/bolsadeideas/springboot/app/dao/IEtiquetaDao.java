package com.bolsadeideas.springboot.app.dao;

import org.springframework.data.repository.CrudRepository;

import com.bolsadeideas.springboot.app.entity.Cliente.Etiqueta;

public interface IEtiquetaDao extends CrudRepository<Etiqueta, Long>{

}
