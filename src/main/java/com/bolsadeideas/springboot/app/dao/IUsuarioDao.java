package com.bolsadeideas.springboot.app.dao;

import org.springframework.data.repository.CrudRepository;

import com.bolsadeideas.springboot.app.entity.Usuario;

//Usuario -> Clase entity , Long -> Tipo del ID
public interface IUsuarioDao extends CrudRepository<Usuario, Long>{
	
	//esto seria lo mismo que una consulta SQL = "select u from Usuario u where u.username=?"
	public Usuario findByUsername(String username);

}
