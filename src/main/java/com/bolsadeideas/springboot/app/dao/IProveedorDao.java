package com.bolsadeideas.springboot.app.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.bolsadeideas.springboot.app.entity.Producto;
import com.bolsadeideas.springboot.app.entity.Proveedor;

public interface IProveedorDao extends CrudRepository<Proveedor, Long>{

	//esto me puede servir para la busqueda por producto no asociado
	@Query("select p from Producto p join fetch p.productoProveedor pp where pp.proveedor_id=?1")
	public List<Producto> findByProveedorId(Long term);
}
