package com.bolsadeideas.springboot.app.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.bolsadeideas.springboot.app.entity.ProductoProveedor;
import com.bolsadeideas.springboot.app.entity.Proveedor;

public interface IProductoProveedorDao extends CrudRepository<ProductoProveedor, Long>{
	
	
	//Metodo que trae todos los productoProveedores asociados a un producto
	@Query("select pp from ProductoProveedor pp where pp.producto_id=?1 ")
	public List<ProductoProveedor> fetchProductoProveedoresByProducto(Long ProductoID);
	
	//Metodo que trae todos los productoProveedores NO asociados a un producto
	@Query("select pp from ProductoProveedor pp where pp.producto_id!=?1 ")
	public List<ProductoProveedor> fetchProductoProveedoresByProductoNoAsociados(Long ProductoID);
	
	//Trae los precios asociados a un producto y proveedor en particular
	@Query("select pp from ProductoProveedor pp where pp.producto_id=?1 and pp.proveedor_id=?2")
	public ProductoProveedor fetchProductoProveedorByProductoIdAndProveedorId(Long ProductoID , Long ProveedorID);
	
	//metodo que trae todos los productoProveedores asociados a un proveedor
	@Query("select pp from ProductoProveedor pp where pp.producto_id=?1 ")
	public List<ProductoProveedor> fetchProductoProveedoresByProveedorID(Long ProveedorID);
	
	
	//metodo para traer los proveedores asociados al producto
	//@Query("select prov from Proveedor prov ")
	//public Proveedor fetchProveedorAsociadoProducto(Long ProductoId);
	
	//metodo para traer los proveedores NO asociados al producto(?)
	@Query("select prov from Proveedor prov left join fetch prov.productoProveedor pp where pp.producto_id!=?1 or pp.producto_id is null group by prov.id")
	public List<Proveedor> fetchProveedorNoAsociadoProducto(Long ProductoId);
	
	
}
