package com.bolsadeideas.springboot.app.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.bolsadeideas.springboot.app.entity.Cliente.RegistroEtiqueta;

public interface IRegistroEtiquetaDao extends CrudRepository<RegistroEtiqueta, Long>{

	//Obtiene Los RegistroEtiquetas asociados a un producto 
	@Query("select reg from RegistroEtiqueta reg where reg.producto_id=?1")
	public List<RegistroEtiqueta> findRegistroEtiquetasByProductoId(Long ProductoID);
	
	//Obtiene Un RegistroEtiqueta por ProductoId y EtiquetaId
	@Query("select reg from RegistroEtiqueta reg where reg.producto_id=?1 and reg.etiqueta_id=?2")
	public RegistroEtiqueta findRegistroEtiquetaByProductoIDAndEtiquetaID(Long ProductoID , Long EtiquetaID);
	
}
