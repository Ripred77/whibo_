package com.bolsadeideas.springboot.app.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.bolsadeideas.springboot.app.entity.Producto;
import com.bolsadeideas.springboot.app.entity.Proveedor;

public interface IProductoDao extends PagingAndSortingRepository<Producto, Long>{
	
	
	//select alias from ClaseEntity , el signo ? referencia al parametro en este caso a term
	//aqui estoy obteniendo una lista de proveedores que se obtienen a traves de la lista de proveedores asociados a determinado producto
	//estas id se encuentran asociadas en ProductoProveedor
	@Query("select p from Proveedor p join fetch p.productoProveedor pp where pp.producto_id=?1")
	public List<Proveedor> findByProductoId(Long term);
	
	/* el JOIN FETCH es un inner join por lo tanto si no existe relacion alguna no traerá ningun dato, 
	 * en caso de necesitar un left join simplemente se agrega left , quedaria: left join fetch
	 * */
	

	public List<Producto> findByNombreProductoLikeIgnoreCase(String term);
	
	public Page<Producto> findByNombreProductoLikeIgnoreCase(String term, Pageable pageable);

	@Query("select p from Producto p join RegistroEtiqueta reg ON p.id = reg.producto_id join ProductoProveedor pp ON p.id = pp.producto_id where reg.etiqueta_id=?1")
	public List<Producto> fetchByIdWhithRegistroEtiquetaWithProductoProveedor(Long EtiquetaID);
	
	@Query("select p.id, p.foto, p.nombreProducto, p.descripcion , p.TotalVenta from Producto p left join RegistroEtiqueta reg ON p.id = reg.producto_id join ProductoProveedor pp ON p.id = pp.producto_id group by p.id")
	public List<Object[]> fetchByIdWhithRegistroEtiquetaWithProductoProveedorTwo();
		
	//Actualmente se usa este en el inicio
	@Query("select p.id, p.foto, p.nombreProducto, p.descripcion, p.TotalVenta from Producto p join RegistroEtiqueta reg ON p.id = reg.producto_id where reg.etiqueta_id=?1 group by p.id")
	public Page<Object[]> fetchByIdWhithRegistroEtiquetaWithProductoProveedorTwoPageable(Long EtiquetaID, Pageable pageable);
	
	
	@Query("select p from Producto p join fetch p.registroEtiqueta reg where reg.etiqueta_id=?1 group by p.id")
	public List<Producto> fetchByIdWhithEtiquetaOrderById(Long EtiquetaID, Pageable pageable);
	
	/*
	@Query("select p from Producto join RegistroEtiqueta reg ON p.id = reg.producto_id where reg.etiqueta_id=?1 group by p.nombre_producto")
	public Page<Producto> fetchProductoByIdWhithEtiquetaOrderByPrecio(Long EtiquetaID, Pageable pageable);
	*/
}
