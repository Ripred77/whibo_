package com.bolsadeideas.springboot.app.controller;

import java.io.IOException; 
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.entity.HelperProveedorPrecio;
import com.bolsadeideas.springboot.app.entity.Producto;
import com.bolsadeideas.springboot.app.entity.Proveedor;
import com.bolsadeideas.springboot.app.service.IProductoProveedorService;
import com.bolsadeideas.springboot.app.service.IProductoService;
import com.bolsadeideas.springboot.app.service.IUploadFileService;
import com.bolsadeideas.springboot.app.service.Cliente.IEtiquetaService;
import com.bolsadeideas.springboot.app.util.paginator.PageRender;

@Controller
@Secured("ROLE_USER")
@RequestMapping(value="productos")
@SessionAttributes("producto")
public class ProductoController {

	@Autowired
	private IProductoService ProductoService;
	
	@Autowired
	private IUploadFileService uploadFileService;
	
	@Autowired
	private IProductoProveedorService productoProveedorService;
	
	@Autowired
	IEtiquetaService EtiquetaService;


	@GetMapping(value = "/uploads/{filename:.+}")
	public ResponseEntity<Resource> verFoto(@PathVariable String filename) {
		
		Resource recurso = null;
		
		try {
			recurso = uploadFileService.load(filename);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + recurso.getFilename() + "\"")
				.body(recurso);
	}
	
	
	@GetMapping(value = "/ver/{id}")
	public String ver(@PathVariable(value="id") Long id, Map <String , Object> model, RedirectAttributes flash) {
		
		Producto producto = ProductoService.findOne(id);
		if(producto == null) {
			flash.addFlashAttribute("error", "El producto no existe en Base de datos");
			return "redirect:/productos";
		}
		
		model.put("producto", producto);
		model.put("titulo", "Detalle producto: " + producto.getNombreProducto());		
		
		ArrayList<HelperProveedorPrecio> Lista = new ArrayList<HelperProveedorPrecio>();
		
		for (Proveedor proveedor :ProductoService.findProveedorbyProducto(id)) {
			HelperProveedorPrecio ProdPrec = new HelperProveedorPrecio();
			ProdPrec.setProductoProveedorID(productoProveedorService.findPreciosByProductoIdAndProveedorId(producto.getId(), proveedor.getId()).getId());
			ProdPrec.setNombreProveedor(proveedor.getRazonSocial());
			ProdPrec.setDireccion(proveedor.getDireccion());
			
			ProdPrec.setPrecio(producto.getTotalVenta());

			Lista.add(ProdPrec);
		}
		
		model.put("proveedoresAsociados",Lista);
		return "productos/ver";	
	}
	
	
	@RequestMapping(value= {"/", ""})
	public String listaProductos(@RequestParam(name="page", defaultValue = "0") int page , Map<String, Object> model) {
		model.put("titulo", "Lista Productos");
		
		Pageable pageRequest = PageRequest.of(page, 5);
		
		Page<Producto> productos =ProductoService.findAll(pageRequest);
		
		PageRender<Producto> pageRender = new PageRender<>("/productos", productos);
		
		model.put("productos", productos);
		model.put("page", pageRender);
		
		return "productos/productos";
	}
	
	@RequestMapping(value= {"/listar/{NombreProducto}"})
	public String listaProductos(@RequestParam(name="page", defaultValue = "0") int page , Map<String, Object> model,
			@PathVariable(value="NombreProducto") String nombreProducto) {
		model.put("titulo", "Lista Productos");
		
		Pageable pageRequest = PageRequest.of(page, 5);
		
		Page<Producto> productos =ProductoService.findByName(nombreProducto, pageRequest);
		
		PageRender<Producto> pageRender = new PageRender<>("/productos", productos);
		
		model.put("productos", productos);
		model.put("page", pageRender);
		
		return "productos/productos";
	}
	
	 
	
	@RequestMapping(value="/productosform")
	public String crear(Map<String , Object>model) {
		
		Producto producto = new Producto();
		model.put("producto", producto);
		//model.put("ListadoEtiquetas", EtiquetaService.findAll());
		model.put("titulo", "Crear Producto");
		
		return "productos/ProductosForm";
	}
	
	@RequestMapping(value="/productosform" , method=RequestMethod.POST )
	public String guardar(@Valid Producto producto, BindingResult result , Model model ,
		@RequestParam("file") MultipartFile foto ,RedirectAttributes flash, SessionStatus status) {
		
		if(result.hasErrors()) {
			model.addAttribute("titulo", "form productos");
			return "productos/ProductosForm";
		}
		
		if (!foto.isEmpty()) {
			
			if(producto.getId() != null && producto.getId() > 0 && producto.getFoto() != null 
					&& producto.getFoto().length() > 0) {
				uploadFileService.delete(producto.getFoto());
			}
			
			String uniqueFileName = null;
			
			try {
				uniqueFileName = uploadFileService.copy(foto);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			flash.addFlashAttribute("info", "has subido correctamente  '" + uniqueFileName + "'");
			
			producto.setFoto(uniqueFileName);
			
		}
		
		String mensajeFlash =(producto.getId() != null)? "Producto editado con exito" : "Producto creado con exito";
		
		ProductoService.save(producto);
		//para que la sesion se elimine o al menos ese parametro(?)
		status.setComplete();
		flash.addFlashAttribute("success" , mensajeFlash);
		
		return "redirect:/productos";
	}
	
	
	@RequestMapping(value="/productosform/{id}")
	public String editar(@PathVariable(value="id") Long id , Map<String , Object> model, RedirectAttributes flash ) {
		
		Producto producto = new Producto();
		
		if (id > 0) {
			producto = ProductoService.findOne(id);
			if(producto == null) {
				flash.addFlashAttribute("error","Producto no existe en la base de datos");
				return "redirect:/productos";
			}
		}else {
			flash.addFlashAttribute("error", "el id del cliente no puede ser 0");
			return "redirect:/productos";
		}

		model.put("producto", producto);
		model.put("ListadoEtiquetas", EtiquetaService.findAll());
		model.put("titulo", "editar producto");
		
		return "productos/ProductosForm";
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/eliminar/{id}")
	public String eliminar(@PathVariable(value="id") Long id, RedirectAttributes flash) {
		
		if(id > 0) {
			ProductoService.delete(id);
			flash.addFlashAttribute("success", "producto eliminado con exito");
		}
		
		return "redirect:/productos";
	}
	
	

	@GetMapping(value="/cargar-productos/{term}" , produces = {"application/json"})
	public @ResponseBody List<Producto> cargarProducto(@PathVariable(value="term") String term){		
		return ProductoService.findByName(term);
	}
	
	
	
}
