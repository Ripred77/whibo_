package com.bolsadeideas.springboot.app.controller.Cliente;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.entity.Producto;
import com.bolsadeideas.springboot.app.entity.Cliente.Etiqueta;
import com.bolsadeideas.springboot.app.entity.Cliente.RegistroEtiqueta;
import com.bolsadeideas.springboot.app.service.Cliente.IEtiquetaService;
import com.bolsadeideas.springboot.app.service.Cliente.IRegistroEtiquetaService;

@Controller
@RequestMapping(value="etiquetas")
@SessionAttributes({"etiqueta" , "producto"})
public class EtiquetasController {
	
	@Autowired
	IEtiquetaService EtiquetaService;
	
	@Autowired
	IRegistroEtiquetaService RegistroEtiquetaService;
	
	private final Logger log = LoggerFactory.getLogger(getClass());

	@RequestMapping(value = {"" , "/" , "/formetiqueta"})
	public String listaEtiquetas(Map<String, Object> model) {
		
		Etiqueta etiqueta = new Etiqueta();
		
		model.put("titulo", "Creacion de etiqueta para producto");
		model.put("etiqueta", etiqueta);
		model.put("ListadoEtiquetas", EtiquetaService.findAll());
		
		return "productos/formEtiquetas";
	}
	
	
	@RequestMapping(value = "/formetiqueta" , method = RequestMethod.POST)
	public String guardarEtiqueta(@Valid Etiqueta etiqueta, BindingResult result, Map<String , Object> model, 
			RedirectAttributes flash , SessionStatus status) {
		
		if(result.hasErrors()) {
			model.put("titulo", "form productos");
			return "productos/formEtiquetas";
		}			
		
		String mensajeFlash = (etiqueta.getId() != null)? "Etiqueta editada con exito" : "Etiqueta creada con exito";
		
		EtiquetaService.save(etiqueta);
		
		status.setComplete();
		
		flash.addFlashAttribute("success",mensajeFlash);
		
		return "redirect:/etiquetas";
	}
	
	
	@RequestMapping(value = "/editaretiqueta/{id}")
	public String editarEtiqueta(@PathVariable(value="id") Long etiquetaId,  Map<String, Object> model) {
		
		Etiqueta etiqueta = EtiquetaService.findOne(etiquetaId);
		
		model.put("titulo", "Editando la etiqueta: " + etiqueta.getNombreEtiqueta());
		model.put("etiqueta", etiqueta);
		model.put("ListadoEtiquetas", EtiquetaService.findAll());
		
		return "productos/etiquetas";
	}
	
	
	//*******************TRATAMIENTO DE LOS REGISTROS DE ETIQUETAS POR PRODUCTO***********************
	
	
	/*la idea es que mediante Jquery el usuario haga check en un checkbox y este genere o elimine el registro
	en cuanto a la eliminacion debe arrojar un error si la etiqueta se está usando
	debe obtener el productoId y la etiquetaId para generar el registro*/
	@GetMapping(value = "/crea-registro-etiqueta/{etiquetaId}", produces = {"application/json"})
	public @ResponseBody List<Etiqueta> HandlerSaveRegistroEtiqueta(@PathVariable(value="etiquetaId") Long EtiquetaId,
			@ModelAttribute("producto") Producto producto) {
		
		RegistroEtiqueta registroEtiqueta = new RegistroEtiqueta();
		registroEtiqueta.setEtiqueta_id(EtiquetaId);
		registroEtiqueta.setProducto_id(producto.getId());
		
		RegistroEtiquetaService.save(registroEtiqueta);
		
		return EtiquetaService.findAll();
	}
	
	//Obtiene una lista de los RegistroEtiquetas filtrando por el Producto Id
	@GetMapping(value="/get-registros-etiquetas" , produces = {"application/json"})
	public @ResponseBody List<RegistroEtiqueta> HandlerGetRegistroEtiqueta(@ModelAttribute("producto") Producto producto){
		return RegistroEtiquetaService.findByProductoID(producto.getId());
	}
	
	//Elimina el registroEtiqueta relacionando al producto y a la etiqueta indicada
	@GetMapping(value = "/elimina-registro-etiqueta/{etiquetaId}", produces = {"application/json"})
	public @ResponseBody List<Etiqueta> HandlerDeleteRegistroEtiqueta(@PathVariable(value="etiquetaId") Long EtiquetaId,
			@ModelAttribute("producto") Producto producto) {
		
		RegistroEtiqueta registroEtiqueta = RegistroEtiquetaService.findByProductoIDAndEtiquetaID(producto.getId(), EtiquetaId);
		
		RegistroEtiquetaService.delete(registroEtiqueta.getId());;
		
		return EtiquetaService.findAll();
	}
	
}
