package com.bolsadeideas.springboot.app.controller;
 
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.entity.HelperProductoPrecio;
import com.bolsadeideas.springboot.app.entity.Producto;
import com.bolsadeideas.springboot.app.entity.ProductoProveedor;
import com.bolsadeideas.springboot.app.entity.Cliente.Etiqueta;
import com.bolsadeideas.springboot.app.service.IProductoProveedorService;
import com.bolsadeideas.springboot.app.service.IProductoService;
import com.bolsadeideas.springboot.app.service.Cliente.IEtiquetaService;
import com.bolsadeideas.springboot.app.util.paginator.PageRender;

@Controller
@RequestMapping(value= {"inicio" , ""})
public class InicioController {
	
	@Autowired
	IProductoService ProductoService;
	
	@Autowired
	IProductoProveedorService productoProveedorService;
	
	@Autowired
	IEtiquetaService etiquetaService;
	
	protected final Log logger = LogFactory.getLog(this.getClass());
	
	//private final Logger log = LoggerFactory.getLogger(getClass());
	
	@RequestMapping(value= {"/", ""})
	public String inicio(@RequestParam(name="page" , defaultValue = "0") int page , Map<String , Object> model) {
		
		//obtiene la autenticacion del usuario, es decir que rol utiliza
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		if(auth != null) {
			logger.info("El usuario autenticado es: ".concat(auth.getName()));
		}
		
		Pageable pageRequest = PageRequest.of(page, 2);
		
		Page<Object[]> results = ProductoService.findProductoInicioTwoPageable(Long.valueOf(1) , pageRequest);
		
		PageRender<Object[]> pageRender = new PageRender<>("/inicio", results);
		
		List<Etiqueta> etiquetas = etiquetaService.findAll();
		
		model.put("titulo", "Inicio");
		model.put("productos", results);
		model.put("etiquetas", etiquetas);
		model.put("page", pageRender);
		model.put("filtroID", 1);
		
		return "inicio/inicio";
	}
	
	//Api que retorna la lista de productos segun la etiqueta
	@GetMapping(value = "/cambia-etiqueta-order-by/{etiquetaID}") //, produces = {"application/json"})
	public String HandlerFiltroInicio(@PathVariable(value="etiquetaID") Long EtiquetaID,
			@RequestParam(name="page" , defaultValue = "0") int page ,
			Map<String, Object> model) {
		
		//obtiene la autenticacion del usuario, es decir que rol utiliza
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		if(auth != null) {
			logger.info("El usuario autenticado es: ".concat(auth.getName()));
		}
		
		Pageable pageRequest = PageRequest.of(page, 2);
		
		Page<Object[]> results = ProductoService.findProductoInicioTwoPageable(EtiquetaID , pageRequest);
		
		PageRender<Object[]> pageRender = new PageRender<>("/inicio", results);
		
		List<Etiqueta> etiquetas = etiquetaService.findAll();
		
		model.put("titulo", "Inicio");
		model.put("productos", results);
		model.put("etiquetas", etiquetas);
		model.put("page", pageRender);
		
		return "inicio/inicio";
	}
	
	@RequestMapping(value="/ver/{ProductoID}")
	public String verProducto(@PathVariable(value="ProductoID") Long ProductoID, Map<String, Object> model,
			RedirectAttributes flash) {
		
		Producto producto = ProductoService.findOne(ProductoID);
		
		if(producto == null) {
			flash.addFlashAttribute("error", "El producto no existe en Base de datos");
			return "redirect:/productos";
		}
		
		model.put("producto", producto);
		
		return "inicio/inicio_ver";
	}
	
	
	
	/*********************PAGINAS DE CONTENIDO ESTATICO*************************/
	@RequestMapping(value="/nosotros")
	public String Nosotros() {
		return "inicio/nosotros";
	}
	
	@RequestMapping(value="/contacto")
	public String Contacto() {
		return "inicio/contacto";
	}
	
	@RequestMapping(value="/contacto", method = RequestMethod.POST)
	public String ContactoReenvioMail() {
		
		/*Inserte codigo para reenviar un mail aqui*/
		
		
		return "inicio/contacto";
	}
	/*toda clase rol dentro de spring debe implementar GrantedAuthority interfaz 
	 * Sirve para validar el rol de forma dinamica obteniendo el contexto*/
	private boolean hasRole(String role) {
		
		SecurityContext context = SecurityContextHolder.getContext();
		
		if(context == null) {
			return false;
		}
		
		Authentication auth = context.getAuthentication();
		
		if(auth == null) {
			return false;
		}
		//"? extends" significa que la coleccion puede recibir cualquier objeto que herede o extienda de la clase señalada
		Collection<? extends  GrantedAuthority> authorities = auth.getAuthorities();
		
		//esta forma obiene directamente true o false dependiendo de lo que traiga role
		return authorities.contains(new SimpleGrantedAuthority(role));
		
		/*
		for(GrantedAuthority authority: authorities) {
			if(role.equals(authority.getAuthority())) {
				return true;
			}
		}
		
		return false;*/
		
	}

}
