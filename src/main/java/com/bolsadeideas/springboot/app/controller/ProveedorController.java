package com.bolsadeideas.springboot.app.controller;

import java.util.ArrayList;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.entity.HelperProductoPrecio;
import com.bolsadeideas.springboot.app.entity.Producto;
import com.bolsadeideas.springboot.app.entity.Proveedor;
import com.bolsadeideas.springboot.app.service.IProductoProveedorService;
import com.bolsadeideas.springboot.app.service.IProveedorService;

@Secured("ROLE_ADMIN")
@Controller
@RequestMapping(value="proveedores")
@SessionAttributes("proveedor")
public class ProveedorController {

	@Autowired
	private IProveedorService proveedorService;
	
	@Autowired
	private IProductoProveedorService productoProveedorService;
	@Secured("ROLE_ADMIN")
	@RequestMapping(value= {"/" , ""})
	public String ListaProveedores(Model model) {
		model.addAttribute("titulo", "Lista Proveedores");
		model.addAttribute("proveedores", proveedorService.findAll());
		return "proveedores/proveedores";
	}
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/ver/{id}")
	public String ver(@PathVariable(value="id") Long id, Map<String , Object> model, RedirectAttributes flash) {
		
		
		Proveedor proveedor = proveedorService.findOne(id);
		if(proveedor == null) {
			flash.addFlashAttribute("error", "El Proveedor no existen en base de datos");
			return "redirect:/proveedores";
		}
		
		model.put("proveedor", proveedor);
		model.put("titulo", "Detalles del Proveedor: "+ proveedor.getRazonSocial());
		
		ArrayList<HelperProductoPrecio> Lista = new ArrayList<HelperProductoPrecio>();
		
		for(Producto producto : proveedorService.findProductoByProveedor(id)) {
			HelperProductoPrecio ProdPrecio = new HelperProductoPrecio();
			
			ProdPrecio.setProductoProveedorID(productoProveedorService.findPreciosByProductoIdAndProveedorId(producto.getId(), proveedor.getId()).getId());
			ProdPrecio.setNombreProducto(producto.getNombreProducto());
			ProdPrecio.setFoto(producto.getFoto());
			ProdPrecio.setNeto(productoProveedorService.findPreciosByProductoIdAndProveedorId(producto.getId(), proveedor.getId()).getNeto());
			
			Lista.add(ProdPrecio);
		};
		
		model.put("productosAsociados", Lista);		
		
		return "proveedores/ver";
	}
	
	@RequestMapping(value="/proveedoresform")
	public String crear(Map<String, Object> model) {
		
		Proveedor proveedor = new Proveedor();
		model.put("proveedor", proveedor);
		
		model.put("titulo", "Form Proveedores");
		
		return "proveedores/ProveedoresForm";
	}
	
	
	@RequestMapping(value="/proveedoresform" , method = RequestMethod.POST)
	public String guardar(@Valid Proveedor proveedor, BindingResult result , Model model, RedirectAttributes flash , SessionStatus status ) {
		
		if(result.hasErrors()) {
			model.addAttribute("titulo", "Form Proveedores");
			return "proveedores/ProveedoresForm";
		}
		
		String mensajeFlash = (proveedor.getId() != null)? "Proveedor editado con exito" : "Proveedor creado con exito";
		
		proveedorService.save(proveedor);
		status.setComplete();
		flash.addFlashAttribute("success",mensajeFlash);
		return "redirect:/proveedores";
	}
	
	
	@RequestMapping(value="/proveedoresform/{id}")
	public String editar(@PathVariable(value="id") Long id , Map<String , Object> model, RedirectAttributes flash ) {
		
		Proveedor proveedor = null;
		
		if (id > 0) {
			proveedor = proveedorService.findOne(id);
			if(proveedor == null) {
				flash.addFlashAttribute("error", "Proveedor no existe en base de datos");
				return "redirect:/proveedores";
			}
		}else {
			flash.addFlashAttribute("error", "el id del cliente no puede ser 0");
			return "redirect:/proveedores";
		}
		
		model.put("titulo", "editar proveedor");
		model.put("proveedor", proveedor);
		
		return "proveedores/ProveedoresForm";
	}
	
	
	@RequestMapping(value="/eliminar/{id}")
	public String eliminar(@PathVariable(value="id") Long id, RedirectAttributes flash) {
	
		if(id > 0) {
			proveedorService.delete(id);
			flash.addFlashAttribute("success", "cliente eliminado con exito");
		}
		
		return "redirect:/proveedores"; 
	}
	
	
	
	
	
}
