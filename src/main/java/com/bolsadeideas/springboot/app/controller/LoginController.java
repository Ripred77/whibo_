package com.bolsadeideas.springboot.app.controller;

import java.security.Principal;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class LoginController {

	@GetMapping("/login")
	public String login(@RequestParam(value="error" ,required = false) String error,
			@RequestParam(value="logout" ,required = false) String logout, 
			Model model, Principal principal , RedirectAttributes flash){
		
		//si principal es distinto a null es porque ya habia iniciado sesion
		if(principal != null) {
			flash.addFlashAttribute("info", "Ya ha iniciado sesion antes");
			return "redirect:/inicio/";
		}
		
		if(error != null) {
			model.addAttribute("error" , "Error de login: Usuario o contraseña incorrecta");
		}
		
		if(logout != null) {
			model.addAttribute("success", "ha cerrado correctamente la sesion");
		}
		
		return "login";
	}
	
}
