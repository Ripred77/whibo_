package com.bolsadeideas.springboot.app.controller;

import java.util.ArrayList;
import java.util.Map;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.entity.HelperProductoPrecio;
import com.bolsadeideas.springboot.app.entity.Producto;
import com.bolsadeideas.springboot.app.entity.ProductoProveedor;
import com.bolsadeideas.springboot.app.entity.Proveedor;
import com.bolsadeideas.springboot.app.service.IProductoProveedorService;
import com.bolsadeideas.springboot.app.service.IProductoService;
import com.bolsadeideas.springboot.app.service.IProveedorService;

@Secured("ROLE_ADMIN")
@Controller
@RequestMapping(value="productoproveedor")
@SessionAttributes({"producto" , "productoProveedor","proveedor"})
public class ProductoProveedorController {

	@Autowired
	private IProductoService ProductoService;
	
	@Autowired
	private IProductoProveedorService productoProveedorService;
	
	@Autowired
	private IProveedorService proveedorService; 
	

	private static final Logger log = LoggerFactory.getLogger(ProductoProveedorController.class);

	
	@RequestMapping(value="/agregarproveedor/{id}")
	public String AddProveedorToProducto(@PathVariable(value="id") Long ProductoID, Map<String , Object> model) {
		
		
		if (ProductoID > 0 && ProductoID != null) {
			
			Producto producto = ProductoService.findOne(ProductoID);
			model.put("titulo", "Agregando proveedor al producto: " + producto.getNombreProducto());
			model.put("producto", producto);			
			
			ProductoProveedor productoProveedor = new ProductoProveedor();
			model.put("productoProveedor", productoProveedor);
			model.put("proveedores", productoProveedorService.fetchProveedoresNoAsociados(ProductoID));
			model.put("proveedoresAsociados", ProductoService.findProveedorbyProducto(ProductoID));
			
			return "productos/agregaProveedor";
		}
		
		return "";
	}
	
	//@ModelAttribute hace referencia al objeto producto que se está guardando en la session
	//Se omite el @valid en el ProductoProveedor ya que este necesita el modelo entity completo el cual 
	//no se provee necesariamente en el formulario
	@RequestMapping(value="/agregarproveedor" , method=RequestMethod.POST)
	public String guardar(ProductoProveedor productoProveedor, BindingResult result, Model model ,
			RedirectAttributes flash , SessionStatus status, @ModelAttribute("producto") Producto producto) {

		if(result.hasErrors()) {
			//log.info(result.getAllErrors().toString());
			model.addAttribute("titulo", "Crear Producto.");
			return "productos/ProductosForm";
		}
		
		String mensajeFlash = (productoProveedor.getId() != null)? "Registro editado con exito" : "Registro creado con exito";
		
		productoProveedor.setProducto_id(producto.getId());
		
		productoProveedorService.save(productoProveedor);
		
		status.setComplete();
		flash.addFlashAttribute("success", mensajeFlash);
		
		if(productoProveedor.getId() == null) {
			return "redirect:/productos";	
		}
		
		return "redirect:/productoproveedor/agregarproveedor/" + productoProveedor.getProducto_id();
	}
	
	@RequestMapping(value="/editarproveedor/{id}")
	@Transactional
	public String editar(@PathVariable(value="id") Long ProductoProveedorID, Map <String, Object> model, RedirectAttributes flash) {
		
		ProductoProveedor prodProv = new ProductoProveedor();
		
		if(ProductoProveedorID > 0) {
			prodProv = productoProveedorService.findOne(ProductoProveedorID);
			if(prodProv == null) {
				flash.addFlashAttribute("error" , "La relacion con este proveedor no existe");
				return "redirect:/productos";
			}
		}else {
			flash.addFlashAttribute("error" , "Esta relacion no existe");
			return "redirect:/productos";
		}
		Producto producto = ProductoService.findOne(prodProv.getProducto_id());
		model.put("producto", producto);
		model.put("proveedores", proveedorService.findOne(prodProv.getProveedor_id()));
		model.put("proveedoresAsociados", ProductoService.findProveedorbyProducto(prodProv.getProducto_id()));
		model.put("productoProveedor", prodProv);
		model.put("titulo", "Edicion de relacion producto: " + producto.getNombreProducto());
		
		return "productos/agregaProveedor";
	}
	
	
	/*--------------Vistas de Proveedor-----------*/
	
	@RequestMapping(value="/agregarproducto/{id}")
	public String AgregarProducto(@PathVariable(value="id") Long ProveedorId, Map<String, Object> model) {
		
		ProductoProveedor prodProv = new ProductoProveedor();
		
		Proveedor proveedor = proveedorService.findOne(ProveedorId);
		
		ArrayList<HelperProductoPrecio> Lista = new ArrayList<HelperProductoPrecio>();
		
		for(Producto producto : proveedorService.findProductoByProveedor(ProveedorId)) {
			HelperProductoPrecio ProdPrecio = new HelperProductoPrecio();
			
			ProdPrecio.setProductoProveedorID(productoProveedorService.findPreciosByProductoIdAndProveedorId(producto.getId(), proveedor.getId()).getId());
			ProdPrecio.setNombreProducto(producto.getNombreProducto());
			ProdPrecio.setFoto(producto.getFoto());
			ProdPrecio.setNeto(productoProveedorService.findPreciosByProductoIdAndProveedorId(producto.getId(), proveedor.getId()).getNeto());
			
			Lista.add(ProdPrecio);
		};
		
		model.put("productosAsociados", Lista);	
		model.put("proveedor", proveedorService.findOne(ProveedorId));
		model.put("productoProveedor", prodProv);
		model.put("titulo", "Agregando Productos a: " + proveedor.getRazonSocial());
		
		return "proveedores/agregarProducto";
	}
	
	/*@ModelAttribute hace referencia al objeto producto que se está guardando en la session
	*el producto_id llega a traves de un input oculto
	genera un nuevo registro en productoProveedor desde la vista de proveedores*/
	@RequestMapping(value="/agregarproducto" , method=RequestMethod.POST)
	public String GuardarProducto( ProductoProveedor productoProveedor, BindingResult result,Map<String, Object> model  ,
			RedirectAttributes flash , SessionStatus status, @ModelAttribute("proveedor") Proveedor proveedor ) {

		if(result.hasErrors()) {
			model.put("titulo", "Crear Producto");
			log.info("*****");
			log.info(result.getAllErrors().toString());
			log.info("*****");
			return "productos/";
		}
		
		if(productoProveedor.getId() != null) {
			productoProveedor.setId(productoProveedorService.findPreciosByProductoIdAndProveedorId(productoProveedor.getProducto_id(), proveedor.getId()).getId());	
		}		
		
		String mensajeFlash = (productoProveedor.getId() != null)? "Registro editado con exito" : "Registro creado con exito";
		
		productoProveedor.setProveedor_id(proveedor.getId());
			
		productoProveedorService.save(productoProveedor);
		
		status.setComplete();
		flash.addFlashAttribute("success", mensajeFlash);
		
		if(productoProveedor.getId() == null) {
			return "redirect:/productos";	
		}
			
		return "redirect:/productoproveedor/agregarproducto/" + proveedor.getId();
	}
	
}
