package com.bolsadeideas.springboot.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.bolsadeideas.springboot.app.auth.handler.LoginSuccesHandler;
import com.bolsadeideas.springboot.app.service.JpaUserDetailsService;

@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter{

	@Autowired
	private LoginSuccesHandler loginSuccesHandler;
	
	@Autowired
	private JpaUserDetailsService userDetailsService;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		//permitAll() autoriza el uso de esos archivos a cualquier usuario
		http.authorizeRequests().antMatchers("/" , "/css/**" , "/js/**" , "/images/**/" , "/inicio").permitAll()
		/*.antMatchers("/ver/**").hasAnyRole("USER")
		.antMatchers("/uploads/**").hasAnyRole("USER")
		.antMatchers("/layout/**").hasAnyRole("USER")
		.antMatchers("/productos/**").hasAnyRole("ADMIN")
		.antMatchers("/proveedores/**").hasAnyRole("ADMIN")
		.antMatchers("/factura/**").hasAnyRole("ADMIN")*/
		.anyRequest().authenticated()
		.and()
			.formLogin()
				.successHandler(loginSuccesHandler)
				.loginPage("/login")
			.permitAll()
		.and()
		.logout().permitAll()
		.and().exceptionHandling()
		.accessDeniedPage("/error_403");
	}
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder builder) throws Exception {
		
		//obtiene el usuario y su(s) roles atraves de una consulta sql
		builder.userDetailsService(userDetailsService)
		.passwordEncoder(passwordEncoder);
		
		
		 
		//lo que hace el "::" es obtener el argumento de la expresion lambda y se la pasa
		/*PasswordEncoder encoder = this.passwordEncoder;
		UserBuilder user =  User.builder().passwordEncoder(encoder::encode);
		
		builder.inMemoryAuthentication()
		.withUser(user.username("admin").password("1234").roles("ADMIN", "USER"))
		.withUser(user.username("edgar").password("1234").roles("USER"));*/
	}
	
	
}
