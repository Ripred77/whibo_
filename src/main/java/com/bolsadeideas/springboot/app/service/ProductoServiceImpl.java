package com.bolsadeideas.springboot.app.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bolsadeideas.springboot.app.dao.IProductoDao;
import com.bolsadeideas.springboot.app.entity.Producto;
import com.bolsadeideas.springboot.app.entity.ProductoProveedor;
import com.bolsadeideas.springboot.app.entity.Proveedor;

@Service
public class ProductoServiceImpl implements IProductoService {

	@Autowired
	IProductoDao productoDao;

	@Override
	@Transactional(readOnly=true)
	public List<Producto> findAll() {
		return (List<Producto>) productoDao.findAll();
	}
	
	@Override
	@Transactional(readOnly=true)
	public Page<Producto> findAll(Pageable pageable) {
		return productoDao.findAll(pageable);
	}

	@Override
	@Transactional
	public void save(Producto producto) {
		productoDao.save(producto);
		
	}

	@Override
	@Transactional(readOnly=true)
	public Producto findOne(Long id) {
		return productoDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		productoDao.deleteById(id);
		
	}

	@Override
	@Transactional(readOnly=true)
	public List<Proveedor> findProveedorbyProducto(Long ProductoID) {		
		return productoDao.findByProductoId(ProductoID);
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Producto> findByName(String term){
		return productoDao.findByNombreProductoLikeIgnoreCase("%"+term+"%");
	}
	
	@Override
	@Transactional(readOnly = true)
	public Page<Producto> findByName(String term, Pageable pageable){
		return productoDao.findByNombreProductoLikeIgnoreCase("%"+term+"%", pageable);
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Producto> findProductoInicio(Long EtiquetaID, String OrderBy){	      
	    return productoDao.fetchByIdWhithRegistroEtiquetaWithProductoProveedor(EtiquetaID);
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Object[]> findProductoInicioTwo(){	      
	    return productoDao.fetchByIdWhithRegistroEtiquetaWithProductoProveedorTwo();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Object[]> findProductoInicioTwoPageable(Long EtiquetaID, Pageable pageable) {
		return productoDao.fetchByIdWhithRegistroEtiquetaWithProductoProveedorTwoPageable(EtiquetaID , pageable);
	}
	
}
