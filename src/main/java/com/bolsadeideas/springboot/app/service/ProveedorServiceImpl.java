package com.bolsadeideas.springboot.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bolsadeideas.springboot.app.dao.IProductoProveedorDao;
import com.bolsadeideas.springboot.app.dao.IProveedorDao;
import com.bolsadeideas.springboot.app.entity.Producto;
import com.bolsadeideas.springboot.app.entity.Proveedor;

@Service
public class ProveedorServiceImpl implements IProveedorService{

	@Autowired
	private IProveedorDao proveedorDao;
	
	@Autowired 
	IProductoProveedorDao productoProveedorDao;
	
	@Override
	@Transactional(readOnly=true)
	public List<Proveedor> findAll() {
		return (List<Proveedor>) proveedorDao.findAll();
	}

	@Override
	@Transactional
	public void save(Proveedor proveedor) {
		proveedorDao.save(proveedor);
	}

	@Override
	@Transactional(readOnly=true)
	public Proveedor findOne(Long id) {
		return proveedorDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		proveedorDao.deleteById(id);
	}

	@Override
	public List<Producto> findProductoByProveedor(Long ProveedorID) {
		return proveedorDao.findByProveedorId(ProveedorID);
	}

}
