package com.bolsadeideas.springboot.app.service;

import java.util.ArrayList;
import java.util.List;

import com.bolsadeideas.springboot.app.entity.ProductoProveedor;
import com.bolsadeideas.springboot.app.entity.Proveedor;

public interface IProductoProveedorService {

	
	public List<ProductoProveedor> findAll();
	
	public void save(ProductoProveedor productoProveedor);
	
	public ProductoProveedor findOne(Long id);
	
	public void delete(Long id);
	
	//trae los productos asociados a un proveedor
	public List<ProductoProveedor> findProductosByProveedor(Long ProveedorID);
	
	//trae los proveedores asociados a un producto
	public List<ProductoProveedor> findProveedorByProducto(Long ProductoID);
	
	public ProductoProveedor findPreciosByProductoIdAndProveedorId(Long ProductoID, Long ProveedorID);
	
	
	public List<Proveedor> fetchProveedoresNoAsociados(Long ProductoID);
}
