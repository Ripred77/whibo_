package com.bolsadeideas.springboot.app.service.Cliente;

import java.util.List;

import com.bolsadeideas.springboot.app.entity.Cliente.RegistroEtiqueta;

public interface IRegistroEtiquetaService {
	
	public List<RegistroEtiqueta> findAll();
	
	public void save(RegistroEtiqueta registroEtiqueta);
	
	public RegistroEtiqueta findOne(Long EtiquetaId);
	
	public void delete(Long EtiquetaId);
	
	public List<RegistroEtiqueta> findByProductoID(Long ProductoId);
	
	public RegistroEtiqueta findByProductoIDAndEtiquetaID(Long ProductoID , Long EtiquetaID);
}
