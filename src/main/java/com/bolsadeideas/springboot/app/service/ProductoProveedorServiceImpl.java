package com.bolsadeideas.springboot.app.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bolsadeideas.springboot.app.dao.IProductoProveedorDao;
import com.bolsadeideas.springboot.app.dao.IProveedorDao;
import com.bolsadeideas.springboot.app.entity.ProductoProveedor;
import com.bolsadeideas.springboot.app.entity.Proveedor;

@Service
public class ProductoProveedorServiceImpl implements IProductoProveedorService{

	@Autowired
	private IProductoProveedorDao productoProveedorDao;
	
	@Autowired
	private IProveedorService proveedorService;
	
	@Override
	@Transactional(readOnly = true)
	public List<ProductoProveedor> findAll() {
		return (List<ProductoProveedor>) productoProveedorDao.findAll();
	}

	@Override
	@Transactional
	public void save(ProductoProveedor productoProveedor) {
		productoProveedorDao.save(productoProveedor);
		
	}

	@Override
	@Transactional(readOnly = true)
	public ProductoProveedor findOne(Long id) {
		return productoProveedorDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		productoProveedorDao.deleteById(id);
	}

	
	//trae los productos asociados a un proveedor
	@Override
	public List<ProductoProveedor> findProductosByProveedor(Long ProveedorID) {
		// TODO Auto-generated method stub
		return null;
	}
	
	//trae los proveedores asociados a un producto
	@Override
	public List<ProductoProveedor> findProveedorByProducto(Long ProductoID) {
		return productoProveedorDao.fetchProductoProveedoresByProducto(ProductoID);
	}

	@Override
	public List<Proveedor> fetchProveedoresNoAsociados(Long ProductoID) {
/*
		ArrayList <Proveedor> ListaProveedores = new ArrayList<Proveedor>();
		
		//trae los productoProveedor asociados a un producto
		for(ProductoProveedor productoProveedor : productoProveedorDao.fetchProductoProveedoresByProductoNoAsociados(ProductoID)) {		
			ListaProveedores.add(proveedorService.findOne(productoProveedor.getProveedor_id()));
		};
		
	*/		
		return productoProveedorDao.fetchProveedorNoAsociadoProducto(ProductoID);
	}

	@Override
	public ProductoProveedor findPreciosByProductoIdAndProveedorId(Long ProductoID, Long ProveedorID) {		
		return productoProveedorDao.fetchProductoProveedorByProductoIdAndProveedorId(ProductoID, ProveedorID);
	}

}
