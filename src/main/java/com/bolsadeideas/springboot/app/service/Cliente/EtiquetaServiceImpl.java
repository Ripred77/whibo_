package com.bolsadeideas.springboot.app.service.Cliente;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bolsadeideas.springboot.app.dao.IEtiquetaDao;
import com.bolsadeideas.springboot.app.entity.Cliente.Etiqueta;

@Service
public class EtiquetaServiceImpl implements IEtiquetaService{
	
	@Autowired
	private IEtiquetaDao etiquetaDao;

	@Override
	public List<Etiqueta> findAll() {
		return (List<Etiqueta>) etiquetaDao.findAll();
	}

	@Override
	public void save(Etiqueta etiqueta) {
		etiquetaDao.save(etiqueta);
	}

	@Override
	public Etiqueta findOne(Long id) {
		return etiquetaDao.findById(id).orElse(null);
	}

	@Override
	public void delete(Long id) {
		etiquetaDao.deleteById(id);
	}

}
