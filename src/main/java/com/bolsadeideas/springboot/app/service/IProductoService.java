package com.bolsadeideas.springboot.app.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.bolsadeideas.springboot.app.entity.Producto;
import com.bolsadeideas.springboot.app.entity.Proveedor;

public interface IProductoService {
	
	public List<Producto> findAll();
	
	public Page<Producto> findAll(Pageable pageable);
	
	public void save(Producto producto);
	
	public Producto findOne(Long id);
	
	public void delete(Long id);
	
	public List<Proveedor> findProveedorbyProducto(Long ProductoID);

	public List<Producto> findByName(String term);
	
	public Page<Producto> findByName(String term, Pageable pageable);
	
	public List<Producto> findProductoInicio(Long EtiquetaID , String OrderBy);
	
	public List<Object[]> findProductoInicioTwo();
	
	public Page<Object[]> findProductoInicioTwoPageable(Long EtiquetaID, Pageable pageable);
}
