package com.bolsadeideas.springboot.app.service;

import java.util.List;

import com.bolsadeideas.springboot.app.entity.Producto;
import com.bolsadeideas.springboot.app.entity.Proveedor;

public interface IProveedorService {
	
	public List<Proveedor> findAll();
	
	public void save(Proveedor proveedor);
	
	public Proveedor findOne(Long id);
	
	public void delete(Long id);
	
	public List<Producto> findProductoByProveedor(Long ProveedorID);
}
