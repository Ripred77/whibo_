package com.bolsadeideas.springboot.app.service.Cliente;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bolsadeideas.springboot.app.dao.IRegistroEtiquetaDao;
import com.bolsadeideas.springboot.app.entity.Cliente.RegistroEtiqueta;

@Service
public class RegistroEtiquetaServiceImpl implements IRegistroEtiquetaService {

	@Autowired
	private IRegistroEtiquetaDao registroEtiquetaDao;
	
	@Override
	public List<RegistroEtiqueta> findAll() {
		return (List<RegistroEtiqueta>) registroEtiquetaDao.findAll();
	}

	@Override
	public void save(RegistroEtiqueta registroEtiqueta) {
		registroEtiquetaDao.save(registroEtiqueta);
	}

	@Override
	public RegistroEtiqueta findOne(Long EtiquetaId) {
		return registroEtiquetaDao.findById(EtiquetaId).orElse(null);
	}

	@Override
	public void delete(Long EtiquetaId) {
		registroEtiquetaDao.deleteById(EtiquetaId);
	}

	@Override
	public List<RegistroEtiqueta> findByProductoID(Long ProductoId) {
		return registroEtiquetaDao.findRegistroEtiquetasByProductoId(ProductoId);
	}

	@Override
	public RegistroEtiqueta findByProductoIDAndEtiquetaID(Long ProductoID, Long EtiquetaID) {
		return registroEtiquetaDao.findRegistroEtiquetaByProductoIDAndEtiquetaID(ProductoID, EtiquetaID);
	}

}
