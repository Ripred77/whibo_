package com.bolsadeideas.springboot.app.service.Cliente;

import java.util.List;

import com.bolsadeideas.springboot.app.entity.Cliente.Etiqueta;

public interface IEtiquetaService {
	
	public List<Etiqueta> findAll();
	
	public void save(Etiqueta etiqueta);
	
	public Etiqueta findOne(Long id);
	
	public void delete(Long id);
	
}
