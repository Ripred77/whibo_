package com.bolsadeideas.springboot.app.view.xlsx;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractXlsxView;

import com.bolsadeideas.springboot.app.entity.Producto;
import com.bolsadeideas.springboot.app.service.IProductoService;
import com.bolsadeideas.springboot.app.service.UploadFileServiceImpl;
import com.lowagie.text.Image;

//la idea es hacer referencia a la extension que tenemos en el application properties, en este caso xlsx
@Component("productos/ver.xlsx")
public class FacturaXlsxView extends AbstractXlsxView{

	@Autowired
	IProductoService productoService;
	
	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		//Crea la plantilla
		Sheet sheet = workbook.createSheet();
		
		//crea una fila
		Row row = sheet.createRow(0);
		//se crea la primera columna a traves de la primera fila(?)
		Cell cell = row.createCell(0);
		cell.setCellValue("Datos Cliente");
		/* Para Crear una segunda columna dentro de la misma fila se haria algo similar a esto
		 * Cell cell2 = row.createCell(1);
		cell2.setCellValue("Algo");*/
		
		row = sheet.createRow(1);
		cell = row.createCell(0); 
		cell.setCellValue("Inserte texto aqui");
		
		row = sheet.createRow(2);
		cell = row.createCell(0);
		cell.setCellValue("Ingrese un segundo texto aqui");
		
		List<Producto> productos = productoService.findAll();
		Producto producto = productos.get(0);
		
		

	}

}
