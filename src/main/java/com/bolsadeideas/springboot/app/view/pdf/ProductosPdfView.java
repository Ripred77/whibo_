package com.bolsadeideas.springboot.app.view.pdf;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import com.bolsadeideas.springboot.app.entity.Producto;
import com.bolsadeideas.springboot.app.service.IProductoService;
import com.bolsadeideas.springboot.app.service.UploadFileServiceImpl;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Image;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

@Component("productos/ver")
//AbstractPdfView esta hereda abstract view
public class ProductosPdfView extends AbstractPdfView {

	@Autowired
	IProductoService productoService;

	@Override
	protected void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		// Obtiene el objeto que se pasa a la vista
		// Producto producto = (Producto) model.get("producto");

		PdfPTable encabezado = new PdfPTable(1);

		PdfPCell monono = new PdfPCell(new Phrase("Monono Pets"));
		monono.setHorizontalAlignment(Element.ALIGN_CENTER);
		encabezado.addCell(monono);

		PdfPCell Articulos = new PdfPCell(new Phrase("Articulos para mascotas"));
		Articulos.setHorizontalAlignment(Element.ALIGN_CENTER);
		encabezado.addCell(Articulos);

		PdfPCell Telefono = new PdfPCell(new Phrase("+569 95 95 28 20 / +569 84 13 30 41"));
		Telefono.setHorizontalAlignment(Element.ALIGN_CENTER);
		encabezado.addCell(Telefono);

		// crea una tabla para insertar los datos
		PdfPTable tabla = new PdfPTable(4);

		// en lugar de obtener el model simplemente use una query
		List<Object[]> productos = productoService.findProductoInicioTwo();

		// necesario para obtener el path de la imagen
		UploadFileServiceImpl servicePath = new UploadFileServiceImpl();
		for (Object[] producto : productos) {
			try {
				
					// obtiene la imagen a traves del path
					Image img1 = Image.getInstance(servicePath.getPath(producto[1].toString()).toString());
					// inserta la imagen a la tabla
					tabla.addCell(img1);

					tabla.addCell("Nombre: \n \n" + producto[2]);
					tabla.addCell("Descripcion: \n \n" + producto[3]);
					tabla.addCell("Precio: \n \n" + producto[4]);

			} catch (Exception e) {
				tabla.addCell("Imagen No Disponible");

				tabla.addCell("Nombre: \n \n" + producto[2]);
				tabla.addCell("Descripcion: \n \n" + producto[3]);
				tabla.addCell("Precio: \n \n" + producto[4]);
			}

		}
		// inserta las tablas al documento es decir lo que se ve en el pdf
		document.add(encabezado);
		document.add(tabla);
		

	}

}
