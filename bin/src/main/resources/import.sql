INSERT INTO proveedores (id, razon_social, direccion) VALUES (1, 'Feng S.A', 'Cerrillos');
INSERT INTO proveedores (id, razon_social, direccion) VALUES (2, 'Lee', 'Estacion central');
INSERT INTO proveedores (id, razon_social, direccion) VALUES (3, 'Venezolanos', 'Meiggs');
INSERT INTO proveedores (id, razon_social, direccion) VALUES (4, 'Chinos lentos', 'Union Lat');
INSERT INTO proveedores (id, razon_social, direccion) VALUES (5, 'Rufus', 'Puente Alto');

INSERT INTO productos (nombre_producto,  foto, descripcion, total_venta) VALUES ( 'Pelota', '' , 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 2000);
INSERT INTO productos (nombre_producto,  foto, descripcion, total_venta) VALUES ( 'Collar', '' , 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 1500);
INSERT INTO productos (nombre_producto,  foto, descripcion, total_venta) VALUES ( 'Rascador', '' , 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 1600);
INSERT INTO productos (nombre_producto,  foto, descripcion, total_venta) VALUES ( 'Arena sanitaria Azul', '' , 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 5000);
INSERT INTO productos (nombre_producto,  foto, descripcion, total_venta) VALUES ( 'Arena sanitaria Rosada', '' , 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 2500);
INSERT INTO productos (nombre_producto,  foto, descripcion, total_venta) VALUES ( 'Filtro', '' , 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',1000);

INSERT INTO producto_proveedor(producto_id, proveedor_id, neto, iva, total, valor_agregado) VALUES ('1','1','1000','190','1190','500');
INSERT INTO producto_proveedor(producto_id, proveedor_id, neto, iva, total, valor_agregado) VALUES ('1','2','1000','190','1190','500');
INSERT INTO producto_proveedor(producto_id, proveedor_id, neto, iva, total, valor_agregado) VALUES ('2','2','1000','190','1190','500');

INSERT INTO etiquetas(nombre_etiqueta) VALUES ("Articulo Perros");
INSERT INTO etiquetas(nombre_etiqueta) VALUES ("Juguete Gatos");
INSERT INTO etiquetas(nombre_etiqueta) VALUES ("Acuario");


INSERT INTO registros_etiquetas(producto_id, etiqueta_id) VALUES (2,1);
INSERT INTO registros_etiquetas(producto_id, etiqueta_id) VALUES (3,2);
INSERT INTO registros_etiquetas(producto_id, etiqueta_id) VALUES (5,3);